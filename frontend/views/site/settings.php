<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \frontend\models\AccountSettings */

$this->title = 'Settings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="col-md-4">

        <div class="avatar-selection-div">
            <img class="img-responsive" id="avatar-img-panel" src="<?= $model->avatar ?>" style="max-height: 190px; max-height: 190px; margin: auto;" alt="">
        </div>


        <label class="control-label">Select File</label>
        
        <input id ="fileLoader" type="file" onchange="previewFile()"><br>
        
        <button type="button" class="btn btn-danger" onclick="removeAvatar()">Remove Avatar</button>

        <div class="height-20"></div>
    </div>

    <div class="col-md-8">
        
        	<form>
	          <div class="cs-input-row" style="width: 200px;">
		        <input id="settings-username" type="text" class="form-control" value="<?= $model->username ?>">
		        <label class="cs-input-label-active">Username</label>
	          </div>
                <div class="height-10"></div>

	          <div class="cs-input-row" style="width: 200px;">
		        <input id="settings-email" type="text" class="form-control" value="<?= $model->email ?>">
		        <label class="cs-input-label-active">email</label>
	          </div>

                <div class="height-20"></div>

	          <div class="cs-input-row" style="width: 200px;">
		        <input id="settings-oldpass" type="password" class="form-control">
		        <label class="cs-input-label-default">Old password</label>
	          </div>
              
                <div class="height-10"></div>

	          <div class="cs-input-row" style="width: 200px;">
		        <input id="settings-newpass1" type="password" class="form-control">
		        <label class="cs-input-label-default">New password</label>
	          </div>
                
                <div class="height-10"></div>

	          <div class="cs-input-row" style="width: 200px;">
		        <input id="settings-newpass2" type="password" class="form-control">
		        <label class="cs-input-label-default">New password again</label>
	          </div>

                <div class="height-20"></div>

	          <div class="cs-input-row" style="width: 200px;">
		        <input id="settings-phonenumber" type="text" class="form-control" value="<?= $model->phonenumber ?>">
		        <label class="cs-input-label-<?= $model->phonenumberExists?'active':'default' ?>">Phone number</label>
	          </div>
	        </form>
        
            <div class="height-20"></div>
        
        <button type="button" class="btn btn-primary" onclick="saveChanges('<?= $model->saveChangesUrl ?>')">Save changes</button>

        <hr />

        Pernametly close account: 
        
        <button type="button" class="btn btn-danger" onclick="confirmDeleteAccount('<?= $model->removeUrl ?>')">Close Account</button>

    </div>
</div>



<script>
    $(document).ready(function () {
        $('.cs-input-row').children('input').each(function (id, value) {
            var left = $(value).position()['left'];
            var x = $(value).next('label');
            x.css({ left: left + 10 });

            // pokud obsahuje text, pak zvedneme
            if (this.value != '') {
                var x = $(this).next('label');
                x.removeClass('cs-input-label-default').addClass('cs-input-label-active');
            }
        });

        $('.cs-input-row').children('input').on("focusout", function () {
            if ($(this).val() == "") {
                var x = $(this).next('label');
                x.removeClass('cs-input-label-active').addClass('cs-input-label-default');
            }
        });

        $('.cs-input-row').children('input').on("change", function () {
            if ($(this).val() != "") {
                var x = $(this).parent().children('input');
                x.focus();
            }
        });

        $('.cs-input-row').children('label.cs-input-label-default').on("click", function () {
            var x = $(this).parent().children('input');
            x.focus();
        });
        $('.cs-input-row').children('input').on("focus", function () {
            var x = $(this).next('label');
            x.removeClass('cs-input-label-default').addClass('cs-input-label-active');
        });

    });

</script>

