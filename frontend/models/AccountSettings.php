<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\helpers\Url;
use common\models\User;

/**
 * Settings
 */
class AccountSettings extends Model
{
    public $reloadNeeded = FALSE;
    public $id;
    public $username;
    public $phonenumber;
    public $avatar;
    public $email;

    public $removeUrl;
    public $saveChangesUrl;

    private $_phonenumberExists;
    private $changes = [];

    public function __construct() {
        $this->removeUrl = Url::to(['site/removeaccount']);
        $this->saveChangesUrl = Url::to(['site/saveaccountchanges']);
    }

    public function loadUser() {
        
        $user = Yii::$app->user->identity;

        $this->username = $user['username'];
        $this->id = $user['id'];

        if (!isset($user['avatar'])) {
            // expand model

            // @todo - need fix, definitly not good idea, to write code like this
            Yii::$app->db->createCommand('ALTER TABLE user ADD COLUMN phonenumber VARCHAR(16) NULL AFTER updated_at, ADD COLUMN avatar BLOB NULL AFTER phonenumber')->execute();
            $user['avatar'] = '';
            $user['phonenumber'] = "";
        }
        
        if ($user['avatar'] == "") 
        {
            $user['avatar'] = 'img/icon-user-default.png';  // default image
        }

        $this->email = $user['email'];
        $this->phonenumber = $user['phonenumber'];
        $this->avatar = $user['avatar'];
    }

    
    public function getPhonenumberExists()
    {
        return strlen(trim($this->phonenumber)) != 0;
    }

    public function saveChanges($username, $password_old, $password_new1, $password_new2, $avatar, $phonenumber, $email)
    {
        $output_status = array('error_status' => false, 'error' => array(), 'reload_needed' => false, 'info' => array());

        $user = User::findOne(Yii::$app->user->identity->id);
        
        $this->loadUser();
        if ($username != $this->username) {
            $user->username = $username;
            $output_status['reload_needed'] = true;
        }
        if ($password_old != "") {
            $passcheck = Yii::$app->getSecurity()->validatePassword($password_old, Yii::$app->user->identity->password_hash);
            if ($password_new1 == $password_new2 && strlen($password_new1 > 5) && $passcheck) {
                
                $user->password_hash = Yii::$app->security->generatePasswordHash($password_new1);
                    
                $output_status['info'][] = 'Password was succesfully changed.';
                $output_status['reload_needed'] = true;
            }
            else 
            {
                if ($password_new1 != $password_new2)
                {
                    $output_status['error'][] = "New passwords mismatch.";
                }
                if (strlen($password_new1 < 6))
                {
                    $output_status['error'][] = "New password is to short.";
                }
                if (!$passcheck) 
                {
                    $output_status['error'][] = "Old password is not valid.";
                }
            }
        }
        if ($avatar != $this->avatar) {
            $user->avatar = $avatar;
            $output_status['reload_needed'] = true;
        }
        if ($phonenumber != $this->phonenumber) {
            $user->phonenumber = $phonenumber;
        }
        if ($email != $this->email) {
            $user->email = $email;
        }

        $output_status['error_status'] = (count($output_status['error']) > 0);

        if (!$output_status['error_status'])
        {
            $user->update();
        }
        
        return $output_status;
    }
    
}
?>
