function confirmDeleteAccount(deleteUrl) {
    bootbox.dialog({
        message: 'Are you sure, you want delete your account?<br />All your data will be lost.<hr /> <div class="checkbox"><label><input type="checkbox" onclick="checkboxTest(this)" value="">I really want remove my account</label></div>',
        title: "Confirm delete account",
        buttons: {
            danger: {
                label: "Remove",
                className: "btn-danger disabled accountRemoveButton",
                callback: function () {
                    if ($('.accountRemoveButton').hasClass('disabled')) {
                        return false;
                    }

                    window.location.replace(deleteUrl);
                }
            },
            success: {
                label: "Cancel",
                className: "btn-default",
                callback: function () {
                }
            }
        }
    });

}

function checkboxTest(node) {
    if ($(node).prop('checked')) {
        $('.accountRemoveButton').removeClass('disabled');
    } else {
        $('.accountRemoveButton').addClass('disabled');
    }
}

function saveChanges(url) {

    var user = {
        username: $('#settings-username').val(),
        phonenumber: $('#settings-phonenumber').val(),
        password_old: $('#settings-oldpass').val(),
        password_n1: $('#settings-newpass1').val(),
        password_n2: $('#settings-newpass2').val(),
        avatar: $('#avatar-img-panel').attr('src'),
        email: $('#settings-email').val()
    };
    $.ajax({
        type: "POST",
        url: url,
        data: user,
        success: function (result) {
            var res = JSON.parse(result);

            if (res.error_status) {
                var msgs = "";
                for (var item in res.error) {
                    msgs += res.error[item] + "<br />";
                }

                bootbox.alert(msgs);
            } else {
                if (res.info.length > 0) {
                    var msgs = "";
                    for (var item in res.info) {
                        msgs += res.info[item] + "<br />";
                    }
                    bootbox.alert(msgs, function() {
                        if (res.reload_needed == true) {
                            document.location.reload();
                        }
                    })
                } else {
                    if (res.reload_needed == true) {
                        document.location.reload();
                    }
                }
            }
        }
    });
    console.log(user);
}

function removeAvatar() { 
    $('#avatar-img-panel').attr('src', 'img/icon-user-default.png');
}

function previewFile() {
    var file = $('#fileLoader').prop("files")[0];
    if (!(file.type == "image/png" || file.type == "image/jpeg")) {
        bootbox.alert("JPEG or PNG is only supported");
        return;
    }
    if (file.size > 32768) { 
        bootbox.alert("Image is too big (max 32Kb)");
        return;
    }

    var fileReader = new FileReader();

    fileReader.addEventListener("load", function () {
        $('#avatar-img-panel').attr('src', fileReader.result);
        $('#avatar-img-panel').css('max-height', '190px');
        $('#avatar-img-panel').css('max-width', '190px');
        $('#avatar-img-panel').css('margin', 'auto');
    }, function() { bootbox.alert('oops!<br />Something is terribly wrong.'); });

    if (file) {
        fileReader.readAsDataURL(file);
    }
}